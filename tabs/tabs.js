const tabButton = document.getElementsByClassName('tab-button');
const tabContent = document.getElementsByClassName('tab-content');

for (let idx1 = 0; idx1 < tabButton.length; idx1++) {
    tabButton[idx1].addEventListener('click', function () {
        for (let idx2 = 0; idx2 < tabButton.length; idx2++) {
            tabButton[idx2].classList.remove('tab-active');
            tabContent[idx2].classList.add('display-none');
        }
        tabButton[idx1].classList.add('tab-active');
        tabContent[idx1].classList.remove('display-none');
    });
}