function togglePopup() {
    const popupButton = document.getElementById('popup-button');
    const popup = document.getElementById('popup');
    const popupClose = document.getElementById('popup-icon-close');

    popupButton.addEventListener("click", function () {
        popup.classList.remove('display-none');
    });

    popupClose.addEventListener("click", function () {
        popup.classList.add('display-none');
    });
}

togglePopup();