function gallery() {
    const galleryImage = document.getElementsByClassName("gallery-image");
    const gallerySource = document.getElementsByClassName("gallery-source");
    const div = document.createElement('div');
    div.className = 'gallery-layer';
    document.body.appendChild(div);

    for (let idx = 0; idx < galleryImage.length; idx++) {
        galleryImage[idx].addEventListener('click', function () {
            const imageSource = gallerySource[idx].srcset;
            div.innerHTML = ``;
            div.innerHTML = `
            <picture class="gallery-image">
            <source srcset="${imageSource}" type="image/jpeg" class="gallery-source">
            <img src="/assets/placeholder.gif" alt="no image">
            </picture>
            `;
        })
    }
}

gallery();

