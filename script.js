var navMenu = document.getElementById("navbar-menu");
var menuIconOpen = document.getElementById("menu-icon-open");
var menuIconClose = document.getElementById("menu-icon-close");

function toggleMenu() {
    navMenu.classList.toggle("display-block");
    menuIconOpen.classList.toggle("display-none");
    menuIconClose.classList.toggle("display-none");
    console.log("clicked")
}

function addButtonBg() {
    var buttonAdd = document.getElementById("button-add");
    buttonAdd.classList.add("bg-gray");
}

function removeButtonBg() {
    var buttonRemove = document.getElementById("button-remove");
    buttonRemove.classList.remove("bg-gray");
}

function toggleButtonBg() {
    var buttonToggle = document.getElementById("button-toggle");
    buttonToggle.classList.toggle("bg-gray");
}