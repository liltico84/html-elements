function toggleModal() {
    const modalButton = document.getElementById('modal-button');
    const modal = document.getElementById('modal');
    const modalClose = document.getElementById('modal-icon-close');

    modalButton.addEventListener("click", function () {
        modal.classList.remove('display-none');
    });

    modalClose.addEventListener("click", function () {
        modal.classList.add('display-none');
    });
}

toggleModal();

function modalFriendButtons() {
    const friendButtons = document.getElementsByClassName('friend-list-item-button');
    const buttonAdd = document.getElementsByClassName('friend-button-add');
    const buttonCheck = document.getElementsByClassName('friend-button-check');

    for (let idx = 0; idx < friendButtons.length; idx++) {
        friendButtons[idx].addEventListener('click', function () {
            friendButtons[idx].classList.toggle('friend-list-item-button-checked');
            buttonAdd[idx].classList.toggle('display-none');
            buttonCheck[idx].classList.toggle('display-none');
        })
    }
}

modalFriendButtons();