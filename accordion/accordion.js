const accordionButton = document.getElementsByClassName('accordion-button');
const accordionIconDown = document.getElementsByClassName('accordion-icon-down');
const accordionIconUp = document.getElementsByClassName('accordion-icon-up');
const accordionContent = document.getElementsByClassName('accordion-content');

for (let idx = 0; idx < accordionButton.length; idx++) {
    accordionButton[idx].addEventListener('click', function () {
        accordionIconDown[idx].classList.toggle('display-none');
        accordionIconUp[idx].classList.toggle('display-none');
        accordionContent[idx].classList.toggle('display-none');
    })
}
