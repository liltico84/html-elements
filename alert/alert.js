const alert = document.getElementById('alert');
const alertButton = document.getElementById('alert-button');
const alertClose = document.getElementById('alert-icon-close');
const alertYes = document.getElementById('alert-yes');
const alertNo = document.getElementById('alert-no');

function toggleAlert() {
    // open alert
    alertButton.addEventListener("click", function () {
       alert.classList.remove('display-none');
    });

    // close alert with icon
    alertClose.addEventListener("click", function () {
        alert.classList.add('display-none');
    });

    // close alert with yes button
    alertYes.addEventListener("click", function () {
        alert.classList.add('display-none');
    });

    // close alert with no button
    alertNo.addEventListener("click", function () {
        alert.classList.add('display-none');
    });
}

toggleAlert();

