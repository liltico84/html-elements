const noticeButton = document.getElementById('notice-button');
const notice = document.getElementById('notice');

noticeButton.addEventListener("click", function () {
    notice.classList.remove('display-none');
    function hideNotice() {
        notice.classList.add('display-none');
    }
    setTimeout(hideNotice, 6000);

    const counter = document.getElementById('counter');
    let count = 6;
    counter.innerText = count;

    let interval = setInterval(function(){

        count -= 1;

        if(count === 1){
            clearInterval(interval);
        }

        counter.innerText = count;
    }, 1000);
});




